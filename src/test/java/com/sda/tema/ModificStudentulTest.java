package com.sda.tema;

import static org.junit.Assert.assertTrue;

import org.junit.Assert;
import org.junit.Test;

import java.util.*;


public class ModificStudentulTest {
    ModificStudentul stud = new ModificStudentul();

    @Test
    public void listTest() {
        System.out.println("Metoda listTest a fost apelata!");

        Student s1 = new Student();
        Student s2 = new Student();
        Student s3 = new Student();

        s1.setFirstName("Andrei");
        s1.setLastName("Seescu");
        s1.setAge(28);
        s1.setSex("Masculin");

        s2.setFirstName("Razvan");
        s2.setLastName("Chitac");
        s2.setAge(25);
        s2.setSex("Masculin");

        s3.setFirstName("Irina");
        s3.setLastName("Moisache");
        s3.setAge(40);
        s3.setSex("Feminin");

        List<Student> actual = new ArrayList<>();

        actual.add(s3);
        actual.add(s1);
        actual.add(s2);
        List<Student> expected = stud.adaugStudent();

        Assert.assertEquals(expected, actual);

    }



    @Test
    public void mapTest() {
        System.out.println("Metoda adaugStudentMap a fost apelata!");

        int id = 0;

        Student student = new Student();
        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();

        student.setFirstName("Vasile");
        student.setLastName("Ursu");
        student.setAge(31);
        student.setSex("Masculin");

        student1.setFirstName("Andrei");
        student1.setLastName("Seescu");
        student1.setAge(28);
        student1.setSex("Masculin");

        student2.setFirstName("Razvan");
        student2.setLastName("Chitac");
        student2.setAge(25);
        student2.setSex("Masculin");

        student3.setFirstName("Irina");
        student3.setLastName("Moisache");
        student3.setAge(40);
        student3.setSex("Feminin");

        Map<Integer, Student> actual = new HashMap<>();
        actual.put(id, student);
        id++;
        actual.put(id, student1);
        id++;
        actual.put(id, student2);
        id++;
        actual.put(id, student3);
        id++;
        actual.put(id, student3);

        Map<Integer, Student> expected = stud.adaugStudentMap();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void listaStudentiSetTest(){
        System.out.println("Metoda listaStudentiSetTest a fost apelata!");

        Set<Student> actual = new HashSet<>();

        Student student = new Student();
        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();

        student.setFirstName("Vasile");
        student.setLastName("Ursu");
        student.setAge(31);
        student.setSex("Masculin");

        student1.setFirstName("Andrei");
        student1.setLastName("Seescu");
        student1.setAge(28);
        student1.setSex("Masculin");

        student2.setFirstName("Razvan");
        student2.setLastName("Chitac");
        student2.setAge(25);
        student2.setSex("Masculin");

        student3.setFirstName("Irina");
        student3.setLastName("Moisache");
        student3.setAge(40);
        student3.setSex("Feminin");

        actual.add(student);
        actual.add(student1);
        actual.add(student2);
        actual.add(student3);
        actual.add(student);  //Adaug acelasi student a doua oara in Set, se va afisa o singura data

        Set<Student> expected = stud.listaStudentiSet();

        Assert.assertEquals(expected, actual);

    }

}
