package com.sda.tema;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ApplicationRunner {
    public static void main(String[] args) {
        ModificStudentul modificStudentul = new ModificStudentul();


        //Afisez studentii din lista (prima metoda din Clasa ModificStudent)
        System.out.println("Afisez studentii din lista: ");
        for (Student element : modificStudentul.adaugStudent()) {
            System.out.print(element.getFirstName() + " " + element.getLastName() + ", " + element.getAge() + " ani, sex " + element.getSex());
            System.out.println();
        }

        System.out.println();

        //Afisez studentii din Map (a doua metoda din Clasa ModificStudent)
        System.out.println("Afisez studentii din Map: ");
        Set<Integer> keys = modificStudentul.adaugStudentMap().keySet();
        for (Integer el : keys) {
            System.out.println(modificStudentul.adaugStudentMap().get(el));
        }
        System.out.println();

        //Afisez studentii din Set (a treia metoda din Clasa ModificStudent. Se afiseaza doar studentii unici)
        System.out.println("Afisez studentii din Set: ");


        for (Student element : modificStudentul.listaStudentiSet()){
            System.out.print(element.getFirstName() + " " + element.getLastName() + ", " + element.getAge() + " ani, sex " + element.getSex());
            System.out.println();
        }

    }
}
