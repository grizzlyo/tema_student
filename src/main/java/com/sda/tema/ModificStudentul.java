package com.sda.tema;

import java.util.*;

public class ModificStudentul {

    // Adaug studenti in lista
    public List<Student> adaugStudent() {
        List<Student> adaugStudent = new ArrayList<>();

        Student student = new Student();
        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();


        student.setFirstName("Vasile");
        student.setLastName("Ursu");
        student.setAge(31);
        student.setSex("Masculin");

        student1.setFirstName("Andrei");
        student1.setLastName("Seescu");
        student1.setAge(28);
        student1.setSex("Masculin");

        student2.setFirstName("Razvan");
        student2.setLastName("Chitac");
        student2.setAge(25);
        student2.setSex("Masculin");

        student3.setFirstName("Irina");
        student3.setLastName("Moisache");
        student3.setAge(40);
        student3.setSex("Feminin");

        adaugStudent.add(student);
        adaugStudent.add(student1);
        adaugStudent.add(student2);
        // sterg student de pe pozitia 1 din lista
        adaugStudent.add(1, student3);

        // sterg obiectul student
        adaugStudent.remove(student);

        // sterg toti studentii din lista
//        adaugStudent.removeAll(adaugStudent);


        return adaugStudent;
    }

    // Adaug studenti in Map
    public Map<Integer, Student> adaugStudentMap() {
        Map<Integer, Student> adaugStudent = new HashMap<>();
        int id = 0;

        Student student = new Student();
        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();

        student.setFirstName("Vasile");
        student.setLastName("Ursu");
        student.setAge(31);
        student.setSex("Masculin");

        student1.setFirstName("Andrei");
        student1.setLastName("Seescu");
        student1.setAge(28);
        student1.setSex("Masculin");

        student2.setFirstName("Razvan");
        student2.setLastName("Chitac");
        student2.setAge(25);
        student2.setSex("Masculin");

        student3.setFirstName("Irina");
        student3.setLastName("Moisache");
        student3.setAge(40);
        student3.setSex("Feminin");

        adaugStudent.put(id, student);
        id++;
        adaugStudent.put(id, student1);
        id++;
        adaugStudent.put(id, student2);
        id++;
        adaugStudent.put(id, student3);
        id++;
        adaugStudent.put(id, student3);


        return adaugStudent;
    }

    //Adaug studenti in set
    public Set<Student> listaStudentiSet(){
        Set<Student> listaStudenti = new HashSet<>();

        Student student = new Student();
        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();

        student.setFirstName("Vasile");
        student.setLastName("Ursu");
        student.setAge(31);
        student.setSex("Masculin");

        student1.setFirstName("Andrei");
        student1.setLastName("Seescu");
        student1.setAge(28);
        student1.setSex("Masculin");

        student2.setFirstName("Razvan");
        student2.setLastName("Chitac");
        student2.setAge(25);
        student2.setSex("Masculin");

        student3.setFirstName("Irina");
        student3.setLastName("Moisache");
        student3.setAge(40);
        student3.setSex("Feminin");

        listaStudenti.add(student);
        listaStudenti.add(student1);
        listaStudenti.add(student2);
        listaStudenti.add(student3);
        listaStudenti.add(student);  //Adaug acelasi student a doua oara in Set, se va afisa o singura data


        return listaStudenti;
    }

}
